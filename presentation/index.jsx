import React from 'react';
import {
  Appear,
  BlockQuote,
  Cite,
  CodePane,
  Deck,
  Fill,
  Fit,
  Heading,
  Image,
  Layout,
  Link,
  List,
  ListItem,
  Markdown,
  Notes,
  Quote,
  Slide,
  Text,
} from 'spectacle';
import createTheme from 'spectacle-theme-nova';
import preloader from 'spectacle/lib/utils/preloader';

const theme = createTheme();

// Import React
// Import Spectacle Core tags
// Import image preloader util
// Import theme
// Require CSS
require("normalize.css");
require("spectacle/lib/themes/default/index.css");

const images = {
  attwood: require("../assets/attwood.png"),
  netscape: require("../assets/netscape.gif"),
  js: require("../assets/javascript.jpg"),
  city: require("../assets/city.jpg"),
  kat: require("../assets/kat.png"),
  logo: require("../assets/formidable-logo.svg"),
  react: require("../assets/reactFirstCode.png"),
  markdown: require("../assets/markdown.png")
};

preloader(images);

const inline = { display: "inline", margin: 0 };
const gutter = "25px";
const transparent = "rgba(0, 0, 0, .5)";

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck
        transition={["zoom", "slide"]}
        transitionDuration={500}
        theme={theme}
        progress="bar"
      >
        <Slide transition={["zoom"]} bgImage="https://cdn-images-1.medium.com/max/1400/1*raWO3dhM4jMjf9VY-kZzNg.png">
          <Heading size={1} fit caps lineHeight={1} bgColor={transparent}>
            JavaScript
          </Heading>
          <Text margin="10px 0 0" size={1} fit bold  bgColor={transparent}>
            Hetkeseis, ecosüsteem
          </Text>
          <Notes>
            {`JavaScript ootamatult on muutunud kõige olulisemaks programmeerimise keeleks maailmas.
             Web-i arengu tõttu JS on igal pool
* browserid
* serverid
* nutiseadmed
* klient rakendused
* IOT

Põjusel et ei ole ühtegi kindlat arendajat areneb kogu ekosüsteem iseenesest.
            `}
          </Notes>
        </Slide>

        <Slide transition={["fade"]} bgImage={images.js.replace("/", "")} >

        </Slide>
        <Slide transition={["fade"]}>
          <Heading size={1} caps>
            Ajalugu
          </Heading>
        </Slide>
        <Slide transition={["fade"]}>
          <Heading size={2} caps>
            Netscape 1994.
          </Heading>
          <Image
            src={images.netscape.replace("/", "")}
            margin="0px auto 40px"
            height="293px"
          />
          <Notes>
            A company called Netscape was founded in 1994 and created one of the
            first web browsers.
          </Notes>
        </Slide>
        <Slide>
          <Heading size={3} textColor="white" caps>
            Brendan Eich 1995
          </Heading>
          <Layout>
            <Fit>
              <Image
                margin={`0 ${gutter} 0 0`}
                src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Brendan_Eich_Mozilla_Foundation_official_photo.jpg/220px-Brendan_Eich_Mozilla_Foundation_official_photo.jpg"
              />
            </Fit>
            <Fill>
              <Text textAlign="left">
                {`Netscape palkas Brendan Eichi 1995, tahtsid et ta teeks programmeerimise liidese nende browserile.`}
                <Markdown
                >{`6 kuu jooksul Mai - December (1995) oli
* Mocha
* LiveScript.
* Detsembri algul 1995, Netscape ja Sun sõlmisid litsentsi lepingu ja nimeks sai JavaScript.`}</Markdown>
              </Text>
            </Fill>
          </Layout>
        </Slide>
        <Slide bgImage="https://i.ytimg.com/vi/6kpxjIbbDT0/maxresdefault.jpg">
        <List >
            <Appear>
              <ListItem bgColor={transparent}>
                IE kopeeris ja nimetas JScript, juriidilistel põhjustel.
              </ListItem>
            </Appear>
            <Appear>
              <ListItem bgColor={transparent}>
                Kõik teised kopeerisid ja jätsid nime JavaScript.
              </ListItem>
            </Appear>
            <Appear>
              <ListItem bgColor={transparent}>
                IE leiutas VBScript, kuid keegi ei kasutanud VBScript-i, juriidilistel põhjustel.
              </ListItem>
            </Appear>
            <Appear>
              <ListItem bgColor={transparent}>Netscape müüdi AOL.</ListItem>
            </Appear>
            <Appear>
              <ListItem bgColor={transparent}>AOL pani kinni Netscape.</ListItem>
            </Appear>
            <Appear>
              <ListItem bgColor={transparent}>
                Ex-Netscape töölised läksid edasi Firefox, SeaMonkey, Chrome etc...
              </ListItem>
            </Appear>
          </List>
        </Slide>

        <Slide
          transition={["fade"]}
          bgImage="https://i.ytimg.com/vi/TloDThABQLA/maxresdefault.jpg"
        />
        <Slide>
        <Image src="https://cdn-images-1.medium.com/max/1600/1*V3Dyq3LSEElX4u3EQTUBrQ.png"/>
        </Slide>

        <Slide>
          <Heading size={3}>Node.js</Heading>
          <Image src="https://camo.githubusercontent.com/dfe125b1579e723de45a206328df7e0705ed9f9f/68747470733a2f2f6e6f64656a732e6f72672f7374617469632f696d616765732f6c6f676f732f6e6f64656a732e706e67"/>
          <Text>
            <Link
              href="https://www.youtube.com/watch?v=ztspvPYybIY"
              target="blank"
            >
              JSConf 2009
            </Link>
          </Text>
          <Notes>
            <Link
              href="https://www.linkedin.com/in/ryan-dahl-a30235b4/"
              target="blank"
            >
              Linked in profile
            </Link>
          </Notes>
        </Slide>
  <Slide>
          <Heading size={3} caps>
            Atwood's Law
          </Heading>
          <Image
            src={images.attwood.replace("/", "")}
            margin="0px auto 40px"
            height="200px"
          />
        </Slide>
        <Slide
          transition={["fade"]}
          bgImage="http://knockoutjs.com/img/main-background.jpg"
        >
          <Heading size={3} textColor="white">
            Knockoutjs
          </Heading>
          <List>
            <ListItem>Declarative bindings</ListItem>
            <ListItem>Automatic UI Refresh</ListItem>
          </List>
          <CodePane
            lang="js"
            source={`There are <span data-bind="text: myItems().length"></span> items`}
          />
          <CodePane
            lang="js"
            source={`<div>
    You've clicked <span data-bind="text: numberOfClicks"></span> times
    <button data-bind="click: incrementClickCounter">Click me</button>
</div>

<script type="text/javascript">
    var viewModel = {
        numberOfClicks : ko.observable(0),
        incrementClickCounter : function() {
            var previousCount = this.numberOfClicks();
            this.numberOfClicks(previousCount + 1);
        }
    };
    ko.applyBindings(viewModel);
</script>`}
          />
        </Slide>
        <Slide>
          <Heading>Raamistike plahvatus</Heading>
          <List>
            <ListItem>October 13, 2010 - Backbone</ListItem>
            <ListItem>October 20, 2010 - Angular</ListItem>
            <ListItem>8 December 2011 - Ember</ListItem>
            <ListItem>...</ListItem>
          </List>
        </Slide>
        <Slide >
          <Heading size={3}>
            React, 2013
          </Heading>
          <Layout>
          <Fit>
          <Image height={400}
                src="https://pbs.twimg.com/media/CxSj3MJWIAA9J1d.jpg"
                margin="0px 40px 0px"
              />
              </Fit>
              <Fit>
          <Image
            src={images.react.replace("/", "")}
            margin="0px 40px 0px"
            height="400px"
          /></Fit>

          </Layout>
          <Link href="https://www.slideshare.net/floydophone/react-preso-v2" target="blank">React demo</Link><br/>
          <Link href="https://signalvnoise.com/posts/3124-give-it-five-minutes" target="blank">Give it 5 minutes</Link>
        </Slide>
        <Slide><Heading size={3}>
            Trendid
          </Heading>
          <Link href="https://github.com/tc39/proposals/blob/master/stage-0-proposals.md" target="blank"/>
        </Slide>

        <Slide><Heading size={3}>
            React Native
          </Heading>
          <Markdown>
          {`
    [React native] (https://facebook.github.io/react-native/)
    [React native boilerplate] (https://github.com/agrcrobles/react-native-web-boilerplate)


          `}
          </Markdown>
        </Slide>

        <Slide><Heading size={3}>
            React Native
          </Heading>
          <Markdown>{`
          - Jooksutab node web serverit mis hoiab kogu React Native JS koodi
          - Tekitab Bridge mis suhtleb JSON abil samasuguse c++ is kirjutatud komponendiga.
          `}</Markdown>
        </Slide>
        <Slide><Heading size={3}>
            Electron
          </Heading>
          <Markdown>{`
          https://electron.atom.io/
          https://github.com/electron/electron-quick-start
          https://electron.atom.io/blog/2017/01/19/simple-samples
          `}</Markdown>
        </Slide>
        <Slide><Heading size={3}>
            Abstract Syntax Tree
          </Heading>
          <Markdown>{`
          https://www.sitepoint.com/understanding-asts-building-babel-plugin/
          `}</Markdown>
        </Slide>


      </Deck>
    );
  }
}
